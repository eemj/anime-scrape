import * as rpn from "request-promise-native";
import scraper from "../dist";

jest.setTimeout(15000);

describe("Structure", () => {
  test('contains a "getDetail" function', () => {
    expect(scraper.getDetail).toBeInstanceOf(Function);
  });

  test('contains a "getDownload" function', () => {
    expect(scraper.getDownload).toBeInstanceOf(Function);
  });

  test('contains a "getList" function', () => {
    expect(scraper.getList).toBeInstanceOf(Function);
  });

  test('contains a "getLatest" function', () => {
    expect(scraper.getLatest).toBeInstanceOf(Function);
  });
});

describe("Latest", () => {
  test("latest animes year should be the current year", async (done) => {
    expect.assertions(1);
    const animeLatest = await scraper.getLatest();
    const animeDate = animeLatest.pop().date.getFullYear();
    expect(animeDate).toEqual(new Date().getFullYear());
    done();
  });
});

describe("Anime1.com Specific Downloads", () => {
  const targetAnime = "one-piece";
  const episodes = ["542", "315", "434"];

  describe("Differentiate between `Subbed/Dubbed/Main/Parts`", () => {
    episodes.forEach((episode) => {
      test(`problematic episode ${episode} should return valid link`, async (done) => {
        const uri = await scraper.anime1.getDownload(targetAnime, episode);
        expect(/^https?:\/\/st\d{1,3}\.anime1\.com/.test(uri)).toBeTruthy();
        done();
      });
    });
  });
});

describe("Downloads", () => {
  test("A random anime's episode's filesize should be greater than 2048 (2MB)", async (done) => {
    const animeLatest = await scraper.getLatest();
    const anime = animeLatest[Math.floor(Math.random() * animeLatest.length) + 1];
    const uri = await scraper.getDownload(anime.link, anime.episode);
    const response = await rpn.head(uri);
    expect(parseInt(response["content-length"], 10)).toBeGreaterThan(2048);
    done();
  });

  test("A non-existant anime should return null", async (done) => {
    const uri = await scraper.getDownload("6265b22b66502d70d5f004f08238ac3c", "1");
    expect(uri).toBeNull();
    done();
  });
});

describe("Detail", () => {
  test('contains "link" and "title"', async (done) => {
    expect.assertions(2);
    const animeDetail = await scraper.getDetail("one-piece");
    const keys = Object.keys(animeDetail);
    expect(keys).toContain("title");
    expect(keys).toContain("link");
    done();
  });
});

describe("List", () => {
  test('contains "link" and "title" in every object', async (done) => {
    const animeList = await scraper.getList();
    for (const anime of animeList) {
      expect(anime).toHaveProperty("title");
      expect(anime).toHaveProperty("link");
    }
    done();
  });
});
