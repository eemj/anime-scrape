import * as rpn from "request-promise-native";

const USER_AGENT: string =
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
const TIMEOUT: number = 20 * 1000;

const request = (host: string, https: boolean = false) =>
  rpn.defaults({
    baseUrl: `http${https ? "s" : ""}://${host}`,
    gzip: true,
    headers: {
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "Accept-Encoding": "gzip, deflate, sdch, br",
      "Accept-Language": "en-US,en;q=0.8",
      "Cache-Control": "no-cache",
      "Host": host,
      "User-Agent": USER_AGENT,
    },
    method: "GET",
    timeout: TIMEOUT,
  });

export default { request };
