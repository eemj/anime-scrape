import * as cheerio from "cheerio";
import * as http from "http";
import * as rpn from "request-promise-native";
import { parse as parseUrl } from "url";
import common from "./common";

const request = common.request("www.anime1.com");

const { load }: CheerioAPI = cheerio;

const anime1Servers: number[] = Array.from(Array(20).keys())
  .map((i) => (++i > 10 ? i + 59 : i))
  .filter((i) => i % 10);

export interface Anime1List {
  [index: string]: any;

  link: string;
  title: string;
}

export interface Anime1Detail {
  [index: string]: any;

  link: string;
  images: string[];
  title: string;
  genre: string[];
  type?: string;
  age_rating?: string;
  status?: string;
  description?: string;
  episodes?: string[];
  synonims?: string;
  prequel?: string;
}

export interface Anime1Latest {
  [index: number]: any;

  link: string;
  title: string;
  date: Date;
  episode: string;
}

export class Anime1 {
  private minContentLength: number;

  constructor(minContentLength: number) {
    this.minContentLength = minContentLength;
  }

  public async getList(): Promise<Anime1List[]> {
    const response = await request("/content/list");
    const query = load(response.toString())(".anime-list li");
    const body: Anime1List[] = [];
    query
      .filter((_, { children }) => children && !!children.length)
      .each((_, { children }) => {
        body.push({
          link: children[0].attribs.href.replace("http://www.anime1.com/watch/", ""),
          title: children[0].children[0].data.trim(),
        });
      });
    return Promise.resolve(body);
  }

  public async getDetail(anime: string): Promise<Anime1Detail> {
    const response = await request(`/watch/${anime}`);
    const parseEpisode = (link: string) => link.match(/\/watch\/.+\/episode-(.*?)$/i)[1];
    const $ = load(response.toString());
    const details: Anime1Detail = {} as Anime1Detail;
    const template = `http://www.anime1.com/main/img/content/${anime}/${anime}-<size>.jpg`;

    details.link = anime;
    details.images = [94, 210].map((size) => template.replace("<size>", `${size}`));
    details.title = $("h1.blue-main-title")
      .first()
      .text()
      .trim();

    $(".detail-left span")
      .map((_, { children }) => children[0])
      .each((_, element) => {
        const detail = element.children[0].data
          .trim()
          .match(/([A-Za-z ]+) :$/i)[1]
          .replace(/\s+/g, "_")
          .toLowerCase();
        if (element.parent.name !== "span") {
          return;
        }
        const elementType = element.next.type.toUpperCase();
        if (elementType === "TEXT") {
          details[detail] = element.next.data.trim();
        } else if (elementType === "TAG") {
          if (element.next.next && element.next.tagName === "a") {
            details[detail] = element.parent.children
              .filter((child) => child.name === "a")
              .map((child) => child.children[0].data.trim());
          } else {
            details[detail] = element.next.children[0].data.trim();
          }
        }
      });

    $("ul.anime-list")
      .last()
      .each((_, { children }) => {
        details.episodes = children
          .filter(
            (child) =>
              child.tagName === "li" &&
              /\/watch\/.+\/episode-(.+?)$/i.test(child.children[0].next.attribs.href),
          )
          .map((child) => parseEpisode(child.children[0].next.attribs.href));
      });

    return Promise.resolve(details);
  }

  public async getDownload(anime: string, episode: string): Promise<string | null> {
    let response: rpn.FullResponse;
    try {
      response = await request(`/watch/${anime}/episode-${episode}`);
    } catch (_) {
      return Promise.resolve(null);
    }

    let buttons = load(response.toString())(".p-left-buttons")[0]
      .children.filter((button) => button.type === "tag")
      .map(({ attribs, children }) => ({
        attribs,
        text: children[0].data,
      }))
      .filter(({ text, attribs }) => !/(Mirror|Dubbed)/g.test(text));

    if (buttons.length) {
      buttons =
        buttons.length >= 2
          ? buttons.sort((a, b) => {
              if (a.attribs && a.attribs.text) {
                return a.attribs.text.localeCompare(b.attribs.text);
              }
              return -1;
            })
          : buttons;

      const isOnSubbed = buttons.find(
        ({ attribs }) => !(attribs.href.endsWith("/1") && attribs.class === "active tabs"),
      );

      if (isOnSubbed) {
        response = await request(parseUrl(buttons[0].attribs.href).path);
      }
    }

    const expression = /file: "(.*?)",/;

    if (expression.test(response.toString())) {
      const result = response.toString().match(expression)[1];

      // Using bruteforce methods to ensure that the download uri works.
      for (const server of anime1Servers) {
        const srvUri = result.replace(/^https?:\/\/st(\d{1,3})\./g, (a: string, b: string) =>
          a.replace(b, server.toString()),
        );

        const valid: boolean = await new Promise((resolve) => {
          const httpReq = http.get(srvUri);
          httpReq.on("response", (res) => {
            if (parseInt(res.headers["content-length"], 10) <= this.minContentLength) {
              return resolve(false);
            }
            resolve(true);
          });
          httpReq.on("error", () => {
            resolve(false);
          });
        });

        if (valid) {
          return Promise.resolve(srvUri);
        }
      }
    }
    return Promise.resolve(null);
  }

  public async getLatest(hours?: number): Promise<Anime1Latest[]> {
    const response = await request(`/latest/episodes`);
    const query = load(response.toString())(".latest-list li");
    const body: Anime1Latest[] = [];
    query
      .filter(
        (_, { children }) =>
          children.length && children[1].type === "tag" && children[1].name === "div",
      )
      .each((_, { children }) => {
        const lastLeft = children[1].children[0];
        const lastRight = children[2].next.children[0];

        const details: Anime1Latest = {} as Anime1Latest;
        details.link = lastLeft.attribs.href;

        // Some of them might be an OVA/Special/Movie and we don't really want that..
        // TODO: Add movie support.. (which should be easy but I'm too lazy)
        if (!/\/episode-/.test(details.link)) {
          return;
        }

        details.link = details.link.match(/http:\/\/www\.anime1\.com\/watch\/(.+?)\//i)[1];
        details.date = new Date(lastRight.attribs.title);
        if (hours) {
          details.date.setHours(details.date.getHours() + hours);
        }
        const title = lastLeft.children[0].data.split(" Episode ");
        details.episode = title[1];
        details.title = title[0];
        body.push(details);
      });
    return Promise.resolve(body.sort((a, b) => a.date.getTime() - b.date.getTime()));
  }
}
