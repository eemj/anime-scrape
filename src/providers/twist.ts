import * as cheerio from "cheerio";
import * as crypto from "crypto";
import * as rpn from "request-promise-native";
import common from "./common";

const request = common.request("twist.moe", true);

const ACCESS_TOKEN = "1rj2vRtegS8Y60B3w3qNZm5T2Q0TN2NR";
const KEY: Buffer = Buffer.from("LXgIVP&PorO68Rq7dTx8N^lP!Fa5sGJ^*XK");

const { load }: CheerioAPI = cheerio;

export interface TwistList {
  [index: string]: string;

  link: string;
  title: string;
}

export interface TwistDetail {
  [index: string]: any;

  link: string;
  title: string;
  episodes: string[];
}

interface EpisodeResponse {
  number: number;
  source: string;
}

export class Twist {
  public async getList(): Promise<TwistList[]> {
    const response: rpn.FullResponse = await request("/");
    const query = load(response.toString())("nav.series > ul.interactive > li");
    const body: TwistList[] = [];
    query
      .filter((_, { children }) => children && !!children.length)
      .each((_, { children }) => {
        body.push({
          link: children[0].attribs.href.split("/")[2],
          title: children[0].children[0].children[0].data.trim(),
        });
      });
    return Promise.resolve(body);
  }

  public async getDetail(anime: string): Promise<TwistDetail> {
    const response: rpn.FullResponse = await request(`/a/${anime}/first`);
    let query = load(response.toString())("div.episode-list > ul > li");
    const episodes: string[] = [];
    query.each((_, { children }) => {
      const episode = children[0].attribs.href;
      if (episode) {
        episodes.push(children[0].attribs.href.split("/").pop());
      }
    });
    query = load(response.toString())("h2.series-title").first();
    const title = query[0].children[0].children[0].data.trim();
    return Promise.resolve({
      episodes,
      link: anime,
      title,
    });
  }

  public async getDownload(anime: string, episode: string | number): Promise<string> {
    let response: EpisodeResponse[];
    try {
      response = await request(`/api/anime/${anime}/sources`, {
        headers: {
          "x-access-token": ACCESS_TOKEN,
        },
        json: true,
      });
    } catch (_) {
      return Promise.resolve(null);
    }

    // if it's a string we have to convert it to an integer
    if (typeof episode === "string") {
      try {
        episode = parseInt(episode.toString(), 10);
      } catch (e) {
        return Promise.reject(e);
      }
    }

    const foundEpisodes = response
      .filter((item: EpisodeResponse) => item.number === episode)
      .map((item: EpisodeResponse) => item.source);

    if (!foundEpisodes.length) {
      return Promise.resolve(null);
    }

    const uri = await this.decrypt(foundEpisodes[0]);
    return Promise.resolve(`http://eu1.twist.moe${uri}`);
  }

  private async decrypt(item: string): Promise<string> {
    const encrypted = Buffer.from(item, "base64");

    if (encrypted.toString("utf8", 0, 8) !== "Salted__") {
      return Promise.reject(new Error("unsupported decryption"));
    }

    const salt = Buffer.from(encrypted.slice(8, 16));
    const data = Buffer.concat([KEY, salt]);

    let keyIV: Buffer;
    {
      let key = crypto
        .createHash("md5")
        .update(data)
        .digest("hex");
      let finalKey: Buffer = Buffer.from(key, "hex");
      while (finalKey.length < 48) {
        key = crypto
          .createHash("md5")
          .update(Buffer.concat([Buffer.from(key, "hex"), data]))
          .digest("hex");
        finalKey = Buffer.concat([finalKey, Buffer.from(key, "hex")]);
      }
      keyIV = finalKey.slice(-48);
    }

    if (keyIV === undefined) {
      return Promise.reject(new Error("unexpected turn of events"));
    }

    const key = Buffer.from(keyIV.slice(0, 32));
    const iv = Buffer.from(keyIV.slice(32));
    const decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
    const decrypt = decipher.update(encrypted.slice(16)) + decipher.final("utf-8");

    return Promise.resolve(decrypt);
  }
}
