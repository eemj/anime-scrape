import * as providers from "./providers";

const MIN_CONTENT_LENGTH = 3 * 1024 * 1024;

class API {
  public anime1 = new providers.Anime1(MIN_CONTENT_LENGTH);
  public twist = new providers.Twist();

  private prioritizeQuality: boolean;
  constructor(prioritizeQuality: boolean = true) {
    this.prioritizeQuality = prioritizeQuality;
  }

  public async getList(): Promise<providers.Anime1List[]> {
    return this.anime1.getList();
  }

  public async getDetail(anime: string): Promise<providers.Anime1Detail> {
    return this.anime1.getDetail(anime);
  }

  public async getLatest(): Promise<providers.Anime1Latest[]> {
    return this.anime1.getLatest();
  }

  public async getDownload(anime: string, episode: string): Promise<string> {
    let uri: string;
    let exception = true;

    try {
      uri = await (this.prioritizeQuality
        ? this.twist
        : this.anime1
      ).getDownload(anime, episode);
      exception = false;
    } catch (e) {
      exception = true;
    } finally {
      if (!uri || exception) {
        uri = await (this.prioritizeQuality
          ? this.anime1
          : this.twist
        ).getDownload(anime, episode);

        return Promise.resolve(uri && uri.length ? uri : null);
      }
    }

    return Promise.resolve(uri);
  }
}

export default new API();
