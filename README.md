# Anime Scraper
*Unofficial Anime1's API*

## Note
- Everything was scraped from 'http://www.anime1.com/'.
- I do not own that site.

## Usage
Typescript/ES6:
```ts
import scraper from 'anime-scrape'
```

CommonJs:
```js
const { default: scraper } = require('anime-scrape')
```
## Documentation

| Method | Parameters | Returns |
| ------ | ---------- | ------- |

- **getList** -> null -> Promise (AnimeList[])
> An array of available animes.
- **getDownload** -> (anime: string, episode: string) -> Promise (string | null)
> A download link for the anime, episode.
- **getDetail** -> (anime: string) -> Promise (AnimeDetail{})
> Brief details for the anime.
- **getLatest** -> (hours?: number) -> Promise (AnimeLatest[])
> An array with latest episodes.

## Example:

```js
const { default: scraper } = require('anime-scrape')

;(async () => {
  const animeList = await scraper.getList()
  console.log(animeList)
})()
```
